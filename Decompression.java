//Author : Keerthi Avittan Elamparithi
//Email id : kelampar@uncc.edu
//Student id : 801166818

import java.io.*;  
import java.util.*; 
import java.lang.Math;

public class Decompression{

	public void decompress (ArrayList list, int bit_length, String file_name)
	{
		try {
			String value= null;
			String string = null;
			String output = "";
			Dictionary dictionary = new Hashtable();
			int count = 256;
			
			// Updating the dictionary with 0-255 ASCII characters
			
			for(int i = 0; i<256; i++)
			{
				char c = (char) i;
				dictionary.put(i,Character.toString(c));
			}
			string = dictionary.get(list.get(0)).toString();
			output=string;
			
			//Updating the table with new elements

			for(int i = 1 ; i < list.size(); i++)
			{
				if(dictionary.get(list.get(i))==null)
				{
					value =string + string.charAt(0);		
				}
				else
				{
					value=dictionary.get(list.get(i)).toString();
				}
				output = output.concat(value);
				if(dictionary.size()<= Math.pow(2,bit_length))
				{
					dictionary.put(count, string+value.charAt(0));
					count = count+1;
					string = value;
				}
				else{
					System.out.println("Dictionary Size Exceeded");
					break;
				}
			}
			
			//Writting the decoded output to <output_filename>.txt 
			
			file_name=(file_name.substring(0,file_name.length() -4)).concat("_decoded.txt");
			File file= new File(file_name);
			if (file.createNewFile())
				System.out.println("File created: " + file.getName());
			else
				System.out.println( file.getName()+" File already exists.");
			
			FileWriter writer =new FileWriter(file_name);
			writer.write(output);
			writer.close();
		}
	
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
	}
	
	
	public static void main(String []args)
	{
		//Main method reads from the encoded file and assigns the contents to an arraylist
		try{
			
			ArrayList newdata = new ArrayList();
			int bit_length=0;
			for(int i =0; i<args.length; i++)
			{	
				if(i== 0)
				{
					File infile = new File(args[i]);
					FileInputStream inputStream = new FileInputStream(infile);
					Reader in = new InputStreamReader(inputStream, "UTF-16BE");
					int read;
					while ((read = in.read()) != -1) {
							newdata.add(read);
						}
					in.close();
				}
				else
				{
					bit_length = Integer.parseInt(args[i]);
				}
			}
		    
			Decompression decomp = new Decompression();
			decomp.decompress(newdata, bit_length, args[0]);
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
	}
	
	
}