//Author : Keerthi Avittan Elamparithi
//Email id :kelampar@uncc.edu
//Student Id : 801166818

import java.io.*;  
import java.util.*; 
import java.lang.Math;
public class Compression {
	
	public int getKey(String value, Dictionary dict)
	{	
		// This method returns the key for the required value/character from the dictionary
		int key = 0;
		Enumeration enu = dict.keys();
		while(enu.hasMoreElements())
		{
			key = (int) enu.nextElement();
			if(dict.get(key).equals(value))
			{
				return(key);
			}
		}
		return 0;	
	}
	
	
	public boolean checkKey(String temp, Dictionary dict)
	{
		// This method checks whether the key is in the dictionary
		int key;
		Enumeration enu = dict.keys();
		
		while(enu.hasMoreElements()) {
			key = (int) enu.nextElement();
			if(dict.get(key).equals(temp))
			{
				return true;
			}	
		}
		return false;
	}

	public int compress(String data, int bit_length, String file_name)
	{	
		// Encodes the data in input file to <output_filename>.lzw
		try{
			int count = 256;
			String value= "";
			String symbol = null;
			Dictionary dictionary = new Hashtable();
			
			//Creating a new file to store the encoded output
			file_name=(file_name.substring(0,file_name.length() -3)).concat("lzw");
			File file= new File(file_name);
			if (file.createNewFile())
				System.out.println("File created: " + file.getName());
			else
				System.out.println( file.getName()+" File already exists.");
			
			//Writing to file in UTF-16BE byte mode

			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file_name), "UTF-16BE"));
			
			// Updating the dictionary with 0-255 ASCII characters
			for(int i = 0; i<256; i++)
			{
				char c = (char) i;
				dictionary.put(i,Character.toString(c));
			}
			
			// Updating the new elements in the table and writing the output to <output_filename>.lzw
			for(int i = 0; i < data.length(); i++)
			{
				symbol = Character.toString(data.charAt(i));
				if(checkKey(value.concat(symbol), dictionary))
				{
					value = value.concat(symbol);
				}
				else
				{	
					if(dictionary.size() <= Math.pow(2,bit_length))
					{
						dictionary.put(count,value.concat(symbol));
						int number = getKey(value, dictionary);
						writer.write(number);
						value = Character.toString(data.charAt(i));
						count=count+1;
					}
					else{
					System.out.println("Dictionary size exceeded");
					break;
					}
				}
			}
				
			int number = getKey(value, dictionary);
			writer.write(number);
			writer.close();
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e);
		}
	
		return 0;
	}
	
	
	
	public static void main(String [] args)
	{
		//Main method reads the data from file and assigns the contents to a string 

		String data = "";
		int bit_length=0;
		try {
   		for(int i =0;i<args.length;i++)
		if(i== 0)
		{
			FileReader file = new FileReader( new File(args[i]));
			BufferedReader br = new BufferedReader(file);
			int c = 0;
			while((c =  br.read()) != -1)
			{
				data = data+ Character.toString((char)c);
			}
		}
		else
		{
			bit_length = Integer.parseInt(args[i]);
		}
		Compression comp = new Compression();	
		comp.compress(data, bit_length, args[0]);
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			e.printStackTrace();
		}
	}
}
