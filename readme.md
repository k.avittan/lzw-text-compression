Author: Keerthi Avittan Elamparithi
Email-id: kelmpar@uncc.edu
Student-id: 801166818

# LZW Compression

The Lempel–Ziv–Welch (LZW) algorithm is a lossless data compression algorithm.

  - Adaptive compression algorithm.
  - Does not assume prior knowledge of the input data.
  - Works well when the input data is sufficiently large and there is redundancy in the data.

## Features!

  - Encoding/Compressing
  - Decoding/Decompressing
  - Dictionary computed during Encoding does not need to be explicitly transmitted / No overhead required.
  - Platform Independant

## Working of LZW Algorithm 

### Encoding

 - Reads the data from an input file.
 - Initializes the dictionary with 0-255 ASCII characters.
 - Reads the string one at a time and appends it to the table only after checking whether the resulting string has a code in the dictionary.
 - Writes the encoded output to the output file.

### Decoding

 - Reads the data from the encoded file.
 - Initializes the dictionary with 0-255 ASCII characters.
 - Reads the input code from encoded sequence and returns a string associated with the sequence.
 - The first character of this new string is concatenated to the previously decoded string.
 - Writes the decoded string to the output file.
 
## Environment

 - Operating System: Windows 10
 - Programming Language: Java
 - JDK version : 13.0.2

## Installation

### Windows

1. Download [Java] JDK 13.
2. Install and set path in environment variables.

### Linux
1. Open command prompt.
2. Run command "sudo apt install openjdk-13-jdk".

## How to run...

### Encoding

```sh
$ javac Compression.java
$ java Compression inputfile.txt bit_length
```

### Decoding

```sh
$ javac Decompression.java
$ java Decompression inputfile.lzw bit_length
```

### Sample Input 

```sh
abbbab
```

### Sample Encoded Output

```sh
 a b 
 ```
 
### Sample Decoded Output

```sh
abbbab
 ```

[Java]: <https://www.oracle.com/java/technologies/javase-jdk13-downloads.html>
  
